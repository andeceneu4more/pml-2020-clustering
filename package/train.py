from __init__ import embeddings_path, labels_path, labels_to_id_path

from sklearn.svm import SVC
from sklearn.preprocessing import Normalizer
from sklearn.model_selection import train_test_split
from sklearn.metrics import f1_score, accuracy_score, pairwise_distances, classification_report
from sklearn.cluster import DBSCAN
from sklearn.manifold import TSNE
from sklearn.metrics.pairwise import cosine_similarity
from minisom import MiniSom

from scipy.spatial.distance import euclidean as euclidean_metric

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
import json

import pdb

def split_data(embeddings_norm, labels):
    """ train-test splitting with a histogram of labels
    """
    X_train, X_test, y_train, y_test = train_test_split(embeddings_norm, labels, shuffle=True, \
                                                        test_size=0.35, random_state=101)
    
    plt.figure(figsize=(15, 10))
    plt.hist(y_train, alpha=0.5, color='blue')
    plt.hist(y_test, alpha=0.5, color='red')
    plt.savefig('resources/split_dist.png')

    return X_train, X_test, y_train, y_test

def train_supervised(X_train, X_test, y_train, y_test):
    """ trains a svm, random forest or ...
    """
    c_range = np.arange(1, 2000, 50)
    best_params = {
        "f1_score_train": 0,
        "f1_score_test": 0
    }

    for c_param in c_range:
        clasifier = SVC(C=c_param, kernel='rbf', gamma='scale', random_state=101, verbose=0)
        clasifier.fit(X_train, y_train)

        train_preds = clasifier.predict(X_train)
        test_preds = clasifier.predict(X_test)

        train_f1 = f1_score(train_preds, y_train, average="micro")
        test_f1 = f1_score(test_preds, y_test, average="micro")

        if best_params["f1_score_test"] < test_f1:
            all_preds = np.append(train_preds, test_preds, axis=0)
            acc_train = accuracy_score(train_preds, y_train)
            acc_test = accuracy_score(test_preds, y_test)
            
            best_params.update({
                "c_param": int(c_param),
                "f1_score_train": train_f1,
                "f1_score_test": test_f1,
                "acc_train": acc_train,
                "acc_test": acc_test,
                "preds": all_preds
            })
        print(f"C_param progress {c_param}", end="\r")

    return best_params, 'svm'

def dbscan_predict(dbscan, X_test, metric):
    """ for each point finds the nearest core closer then the eps distance
    """
    preds = (-1) * np.ones(shape=len(X_test), dtype=int) 

    cores = dbscan.components_
    if cores.size == 0:
        return preds

    if metric == euclidean_metric:
        metric = lambda x, y: np.sqrt(np.sum(np.square(x - y), axis=1))

    for i, x_test in enumerate(X_test):
        distances = metric(x_test, cores)
        j = np.argmin(distances)
        if distances[j] < dbscan.eps:
            preds[i] = dbscan.labels_[dbscan.core_sample_indices_[j]]
    return preds

def plot_kth_distances(X_train, X_test, kth):
    embeddings_norm = np.append(X_train, X_test, axis=0)
    distance_matrix = pairwise_distances(embeddings_norm, embeddings_norm, metric=euclidean_metric, n_jobs=-1)

    kth_neighbour = np.argsort(distance_matrix, axis=1)[:, kth]

    kth_distances = [np.sqrt(np.sum((embeddings_norm[i] - embeddings_norm[j]) ** 2))
                    for i, j in enumerate(kth_neighbour)]
    
    seri = pd.Series(kth_distances)
    rolled_distances = seri.rolling(10).mean().values

    plt.figure(figsize=(15, 10))
    plt.plot(kth_distances)
    plt.plot(rolled_distances)
    plt.legend(['real', 'smoothed'])
    plt.savefig('resources/kth_distances.png')


def train_dbscan(X_train, X_test, y_train, y_test):
    kth = 3
    distance = 0.6
    
    dbscan = DBSCAN(eps=distance, min_samples=kth, metric=euclidean_metric)
    train_preds = dbscan.fit_predict(X_train)
    test_preds = dbscan_predict(dbscan, X_test, euclidean_metric)
    all_preds = np.append(train_preds, test_preds, axis=0)

    train_f1 = f1_score(train_preds, y_train, average="micro")
    test_f1 = f1_score(test_preds, y_test, average="micro")
    acc_train = accuracy_score(train_preds, y_train)
    acc_test = accuracy_score(test_preds, y_test)

    best_params = {
        "f1_score_train": train_f1,
        "f1_score_test": test_f1,
        "acc_train": acc_train,
        "acc_test": acc_test,
        "preds": all_preds
    }
    plot_kth_distances(X_train, X_test, kth)
    return best_params

def plot_pie_map(som, som_shape, X_train, y_train):
    labels_map = som.labels_map(X_train, y_train)
    # with open(labels_to_id_path, 'r') as fin:
    #     labels_to_id = json.load(fin)
    label_names = np.arange(40) #list(labels_to_id.keys())
    fig = plt.figure(figsize=(30, 20))

    the_grid = gridspec.GridSpec(som_shape[1], som_shape[0], fig)
    for position in labels_map.keys():
        label_fracs = [labels_map[position][l] for l in label_names]
        
        plt.subplot(the_grid[som_shape[1] - 1 - position[1], position[0]])
        patches, _ = plt.pie(label_fracs, normalize=True)

    plt.legend(label_names, ncol=3, bbox_to_anchor=(-10, -10))
    plt.savefig('resources/pie_map.png')


def som_predict(som, X_train, y_train, X_test):
    win_map = som.labels_map(X_train, y_train)

    default_class = np.sum(list(win_map.values())).most_common()[0][0]
    preds = []
    for embedding in X_test:
        win_position = som.winner(embedding)
        if win_position in win_map:
            preds.append(win_map[win_position].most_common()[0][0])
        else:
            print(win_position)
            preds.append(default_class)
    return preds


def train_som(X_train, X_test, y_train, y_test):
    som_dimension = 1600
    best_params = {
        "f1_score_train": 0,
        "f1_score_test": 0,
    }
    neurons_range = [80] #np.arange(10, 200, 10)

    for m_neurons in neurons_range:
        n_neurons = som_dimension // m_neurons
        som_shape = (m_neurons, n_neurons)
        
        som = MiniSom(som_shape[0], som_shape[1], input_len=X_train.shape[1], \
                    sigma=1.5, learning_rate=0.5, neighborhood_function='gaussian', \
                    topology='rectangular', activation_distance='euclidean', random_seed=101)
        iterations = 2100
        som.train_batch(X_train, num_iteration=iterations, verbose=True)

        train_preds = som_predict(som, X_train, y_train, X_train)
        test_preds = som_predict(som, X_train, y_train, X_test)
        
        train_f1 = f1_score(train_preds, y_train, average="micro")
        test_f1 = f1_score(test_preds, y_test, average="micro")

        if best_params["f1_score_test"] < test_f1:
            all_preds = np.append(train_preds, test_preds, axis=0)
            acc_train = accuracy_score(train_preds, y_train)
            acc_test = accuracy_score(test_preds, y_test)
            
            best_params.update({
                "m_neurons": int(m_neurons),
                "n_neurons": int(n_neurons),
                "f1_score_train": train_f1,
                "f1_score_test": test_f1,
                "acc_train": acc_train,
                "acc_test": acc_test,
                "preds": all_preds
            })
            all_labels = np.append(y_train, y_test, axis=0)
            # print(classification_report(all_labels, all_preds))
            plot_pie_map(som, som_shape, X_train, y_train)
        print(f"m_neurons progress {m_neurons}")
    
    return best_params

        
def export_t_sne_dataset(embeddings_norm, labels):
    t_sne = TSNE(n_components=2, metric="l2", random_state=101, square_distances=True)
    X_2d = t_sne.fit_transform(embeddings_norm)

    plt.figure(figsize=(15, 10))
    plt.scatter(X_2d[:, 0], X_2d[:, 1], c=labels)
    plt.colorbar()
    plt.savefig('resources/classes_2d.png')
    return X_2d

def plot_and_save_config(config, X_2d, method_name):
    json_file_name = f'config_{method_name}.json'
    all_preds = list(map(int, config.get("preds", [])))
    config["preds"] = all_preds
    
    with open(json_file_name, 'w') as fout:
        json.dump(config, fout)

    all_preds = list(map(int, config.get("preds")))
    plt.figure(figsize=(15, 10))
    plt.scatter(X_2d[:, 0], X_2d[:, 1], c=all_preds)
    plt.colorbar()
    plt.savefig(f'resources/preds_{method_name}.png')
    

def main():
    embeddings = np.load(embeddings_path, allow_pickle=True)
    labels = np.load(labels_path, allow_pickle=True)

    normalizer = Normalizer(norm='l2')
    embeddings_norm = normalizer.fit_transform(embeddings)
    X_2d = export_t_sne_dataset(embeddings_norm, labels)

    X_train, X_test, y_train, y_test = split_data(embeddings_norm, labels)

    supervised_params, model_name = train_supervised(X_train, X_test, y_train, y_test)
    f1_supervised = supervised_params.get("f1_score_test")
    plot_and_save_config(supervised_params, X_2d, model_name)

    som_params = train_som(X_train, X_test, y_train, y_test)
    f1_som = som_params.get("f1_score_test")
    plot_and_save_config(som_params, X_2d, 'som')

    dbscan_params = train_dbscan(X_train, X_test, y_train, y_test)
    f1_dbscan = dbscan_params.get("f1_score_test")
    plot_and_save_config(dbscan_params, X_2d, 'dbscan')

    print(f"F1 Supervised - {model_name} - {f1_supervised}"  + \
          f"F1 SOM {f1_som}"  + \
          f"F1 DBSCAN {f1_dbscan}" )

if __name__ == "__main__":
    main()