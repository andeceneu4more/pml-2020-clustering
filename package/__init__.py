
train_xml_path = 'corpus/RomanianLS_train/RomanianLS.pos.train'
ro_fast_text_path = 'resources/romanian_fast_text.model'
dict_xml_path = 'corpus/RomanianLS_train/RomanianLS.dictionary.xml'

embeddings_path = 'resources/embeddings.npy'
labels_path = 'resources/labels.npy'
labels_to_id_path = 'resources/labels2id.json'

no_needed_poses = {'COLON', 'SCOLON', 'PERIOD', 'COMMA', 'DASH', 'CLEP', \
                   'DBLQ', 'EXCL', 'EXCLHELIP', 'HELIP', 'AMPER', 'LPAR', \
                   'RPAR', 'PLUS', 'QUEST', 'QUOT', 'SLASH'}
embedding_size = 300