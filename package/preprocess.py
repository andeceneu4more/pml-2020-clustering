import re
import os
import json
import numpy as np
from bs4 import BeautifulSoup

from nltk.corpus import stopwords
from gensim.models import FastText

from sklearn.preprocessing import LabelEncoder


from __init__ import train_xml_path, dict_xml_path, no_needed_poses, ro_fast_text_path, \
                      embedding_size, embeddings_path, labels_path, labels_to_id_path

def extract_article_data(article, ro_stopwords):
    """ extracts the text from an ROCO article by ignoring specific 
        part of speech tags (punctions) - no_needed_poses
    """
    article_words = []
    article_poses = []

    arcticle_list = re.split(r'\s+', article)
    arcticle_list = list(filter(lambda elem: elem != '', arcticle_list))
    for article_elem in arcticle_list:
        word, pos = ['/', 'SLASH'] \
                    if article_elem[:2] == '//' \
                    else article_elem.split('/')

        if pos not in no_needed_poses and word not in ro_stopwords:
            article_words.append(word)
            article_poses.append(pos.upper())

    return article_words, article_poses

def extract_text_data():
    """ extracts the text and POSes from multiple articles;
        it also has a limitation of maximum 5000 articles
    """
    ro_stopwords = stopwords.words('romanian')

    with open(train_xml_path, 'r') as fin:
        xml_data = fin.read()
    soup = BeautifulSoup(xml_data, 'lxml')
    instances = list(soup.findAll('instance'))#[:5000]

    words_list = []
    poses_list = []
    for idx, instance in enumerate(instances):
        article = instance.context.text
        article_words, article_poses = extract_article_data(article, ro_stopwords)

        words_list.append(article_words)
        poses_list.append(article_poses)

        print(f"Processing instances {idx}/{len(instances)}", end="\r")
    print()
    return words_list, poses_list

def get_word_embeddings(words_list):
    """ train a fast text model from gensim and stores it locally
    """
    ro_fast = FastText(size=300, \
                window=5, \
                sg=1, \
                word_ngrams=1, \
                workers=4, \
                min_count=3, \
                iter=5)
    if not os.path.isfile(ro_fast_text_path):
        ro_fast.build_vocab(sentences=words_list)
        ro_fast.train(sentences=words_list, total_examples=len(words_list), epochs=10)
        ro_fast.save(ro_fast_text_path)
    else:
        ro_fast = FastText.load(ro_fast_text_path)
    return ro_fast

def clean_gloss(gloss):
    """ clean special characters from gloss
    """
    gloss = re.sub(r'[,.\(\)]', ' ', gloss)
    gloss = re.sub(r'\s+', ' ', gloss)
    return gloss

def extract_dict_gloss(vocab_data, label, lexelt):
    """ extracts words from gloss; if needed some words close to the label are removed
    """
    lexelt_soup = BeautifulSoup(str(lexelt), 'lxml')

    for sense in lexelt_soup.findAll('sense'):
        word = sense['label'].replace('-', '_')
        vocab_data[word] = label

        gloss = clean_gloss(sense['gloss'])
        for word in gloss.split(' '):
            if word not in vocab_data.keys(): # and ro_fast.wv.similarity(word, label) > 0.35:
                vocab_data[word] = label

def get_encoded_data(labels, vocab_data, ro_fast):
    """ encodes the labels and computes a numpy array containing all embeddings
    """
    le = LabelEncoder()
    labels_enc = le.fit_transform(labels)
    label_to_id = {key: int(id) for key, id in zip(labels, labels_enc)}

    embeddings = np.array([]).reshape(-1, embedding_size)
    labels = np.array([], dtype=np.int32)

    for word in vocab_data.keys():
        label_enc = label_to_id[vocab_data[word]]
        labels = np.append(labels, label_enc)

        embedding = ro_fast.wv[word]
        embedding = np.expand_dims(embedding, axis=0)
        embeddings = np.concatenate((embeddings, embedding))
    return embeddings, labels, label_to_id

def extract_dict_data(ro_fast):
    """ extracts the labels and the words from gloss and encodes them to be stored locally
    """
    with open(dict_xml_path, 'r') as fin:
        xml_data = fin.read()
    soup = BeautifulSoup(xml_data, 'lxml')

    vocab_data = {}
    labels = []

    for lexelt in soup.findAll('lexelt'):
        label = lexelt['item'].split('.')[0]
        labels.append(label)
        extract_dict_gloss(vocab_data, label, lexelt)
    
    return get_encoded_data(labels, vocab_data, ro_fast)


def main():
    words_list, _ = extract_text_data()
    ro_fast = get_word_embeddings(words_list)

    embeddings, labels, label_to_id = extract_dict_data(ro_fast)
    np.save(embeddings_path, embeddings, allow_pickle=True)
    np.save(labels_path, labels, allow_pickle=True)

    with open(labels_to_id_path, 'w') as fout:
        json.dump(label_to_id, fout, indent=4, sort_keys=True)


if __name__ == "__main__":
    main()